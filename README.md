# Weather App

Simple mobile application to check weather in your city

## Installation

Clone and use npm or yarn to install dependencies.

```bash
npm install
```

# or

```bash
yarn install
```

For macOS, you should navigate to ios directory

> ~/ios

and then run

```bash
pod install
```

## Run the application
If you want to run your application on xcode simulator you can run your application in your dev environment using

```bash
yarn ios
```

# or

if you using android emulator you can use

```bash
yarn android
```
