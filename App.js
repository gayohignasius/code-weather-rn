import React from 'react';
import Home from './src/screens/Home';
import { Provider } from 'react-redux';
import store from './src/store';

const MainApp = () => {
  return <Home />;
};

const App = () => {
  return (
    <Provider store={store}>
      <MainApp />
    </Provider>
  );
};

export default App;
