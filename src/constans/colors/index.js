export const colors = {
  blue: '#2197f2',
  day: '#2177cf',
  night: {
    blue: '#2e4482',
    pink: '#bea9de',
  },
  black: '#000000',
  white: '#ffffff',
  lightgray: '#f9f9f9',
  card: '#a4c6fc',
};
