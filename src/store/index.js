import { combineReducers, configureStore } from '@reduxjs/toolkit';
import weatherSlice from '../features/weatherSlice';
import forecastSlice from '../features/forecastSlice';

const rootReducer = combineReducers({
  weather: weatherSlice,
  forecast: forecastSlice,
});

const store = configureStore({
  reducer: rootReducer,
});

export default store;
