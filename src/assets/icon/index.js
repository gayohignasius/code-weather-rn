import HumidityIcon from './001-humidity.png';
import ThermometerIcon from './002-thermometer.png';
import AirPressureIcon from './003-air.png';
import WindSpeedIcon from './004-storm.png';
import VisibilityIcon from './005-eye.png';
import SunriseIcon from './006-sunrise.png';
import SunsetIcon from './007-sunset.png';
import SearchIcon from './008-search.png';

export {
  HumidityIcon,
  ThermometerIcon,
  AirPressureIcon,
  WindSpeedIcon,
  VisibilityIcon,
  SunriseIcon,
  SunsetIcon,
  SearchIcon,
};
