import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { API_KEY, API_URL } from '../helper';

export const forecastSlice = createSlice({
  name: 'forecast',
  initialState: {
    status: '',
    forecast: [],
    error: null,
  },
  reducers: {},
  extraReducers: builder => {
    builder.addCase(getForecastAsync.pending, state => {
      state.status = 'loading';
    });

    builder.addCase(getForecastAsync.fulfilled, (state, { payload }) => {
      const arr = payload.list;
      const newArr = arr.slice(0, 24);
      return {
        status: 'fulfilled',
        forecast: newArr,
      };
    });

    builder.addCase(getForecastAsync.rejected, (state, { payload }) => {
      state.status = 'error';
      state.error = payload.error.message;
    });
  },
});

export const getForecastAsync = createAsyncThunk(
  'forecast/getForecast',
  async city => {
    try {
      const { data } = await axios.get(
        `${API_URL}/forecast?q=${city}&units=metric&appid=${API_KEY}`,
      );
      return data;
    } catch (error) {
      throw new Error(error);
    }
  },
);

export default forecastSlice.reducer;
