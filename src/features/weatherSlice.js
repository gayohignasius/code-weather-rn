import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { API_KEY, API_URL } from '../helper';

export const weatherSlice = createSlice({
  name: 'weather',
  initialState: {
    status: '',
    weather: {},
    error: null,
  },
  reducers: {},
  extraReducers: builder => {
    builder.addCase(getWeatherAsync.pending, state => {
      state.status = 'loading';
    });

    builder.addCase(getWeatherAsync.fulfilled, (state, { payload }) => {
      return {
        status: 'fulfilled',
        weather: payload,
      };
    });

    builder.addCase(getWeatherAsync.rejected, (state, { payload }) => {
      state.status = 'error';
      state.error = payload.error.message;
    });
  },
});

export const getWeatherAsync = createAsyncThunk(
  'weather/getWeather',
  async city => {
    try {
      const { data } = await axios.get(
        `${API_URL}/weather?q=${city}&units=metric&appid=${API_KEY}`,
      );
      return data;
    } catch (error) {
      throw new Error(error);
    }
  },
);

export default weatherSlice.reducer;
