import { StyleSheet, Text, View, Image } from 'react-native';
import React, { useEffect } from 'react';
import { colors } from '../../constans/colors';
import {
  AirPressureIcon,
  HumidityIcon,
  SunriseIcon,
  SunsetIcon,
  ThermometerIcon,
  VisibilityIcon,
  WindSpeedIcon,
} from '../../assets';

const WeatherItem = ({ weather }) => {
  const sunrise = new Date(weather.sys.sunrise * 1000);
  const sunset = new Date(weather.sys.sunset * 1000);

  return (
    <>
      {/* main content */}
      <View style={styles.row}>
        <View>
          <Text style={styles.city}>
            {weather !== undefined ? weather.name : 'Jakarta'}
          </Text>
          <Text style={styles.temperature}>
            {weather !== undefined ? Math.round(weather.main.temp) : '27'}
            °C
          </Text>
          <Text style={styles.temperatureText}>
            {weather !== undefined ? weather.weather[0].main : 'Clouds'}
          </Text>
        </View>
        <View>
          <Image
            style={styles.image}
            source={{
              uri: `https://openweathermap.org/img/wn/${weather.weather[0].icon}@4x.png`,
            }}
          />
        </View>
      </View>
      {/* mid content */}
      <View style={styles.row}>
        <View style={styles.info}>
          <View style={styles.card1}>
            <Text style={styles.infoTitle}>Humidity</Text>
            <View style={styles.iconContainer1}>
              <Image source={HumidityIcon} style={styles.icon1} />
            </View>
            <Text style={styles.infoText}>
              {weather !== undefined ? weather.main.humidity : '100'}%
            </Text>
          </View>
        </View>
        <View style={styles.info}>
          <View style={styles.card1}>
            <Text style={styles.infoTitle}>Pressure</Text>
            <View style={styles.iconContainer1}>
              <Image source={AirPressureIcon} style={styles.icon1} />
            </View>
            <Text style={styles.infoText}>
              {weather !== undefined ? weather.main.pressure : '1007'}hPa
            </Text>
          </View>
        </View>
        <View style={styles.info}>
          <View style={styles.card1}>
            <Text style={styles.infoTitle}>Wind Speed</Text>
            <View style={styles.iconContainer1}>
              <Image source={WindSpeedIcon} style={styles.icon1} />
            </View>
            <Text style={styles.infoText}>
              {weather !== undefined ? weather.wind.speed : '1.32'} m/s
            </Text>
          </View>
        </View>
      </View>
      <View style={{ flex: 1 }}>
        <View style={styles.row}>
          <View style={styles.card2}>
            <Text style={styles.cardTitle}>Feels like</Text>
            <View style={styles.iconContainer2}>
              <Image source={ThermometerIcon} style={styles.icon2} />
            </View>
            <Text style={styles.cardInfo}>
              {weather !== undefined
                ? Math.round(weather.main.feels_like)
                : '25'}
              °C
            </Text>
          </View>
          <View style={styles.card2}>
            <Text style={styles.cardTitle}>Visibility</Text>
            <View style={styles.iconContainer2}>
              <Image source={VisibilityIcon} style={styles.icon2} />
            </View>
            <Text style={styles.cardInfo}>
              {weather !== undefined ? weather.visibility / 1000 : '10'} km
            </Text>
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.card2}>
            <Text style={styles.cardTitle}>Sunrise</Text>
            <View style={styles.iconContainer2}>
              <Image source={SunriseIcon} style={styles.icon2} />
            </View>
            <Text style={styles.cardInfo}>
              {sunrise.toLocaleTimeString().replace(/:\d+ /, ' ')}
            </Text>
          </View>
          <View style={styles.card2}>
            <Text style={styles.cardTitle}>Sunset</Text>
            <View style={styles.iconContainer2}>
              <Image source={SunsetIcon} style={styles.icon2} />
            </View>
            <Text style={styles.cardInfo}>
              {sunset.toLocaleTimeString().replace(/:\d+ /, ' ')}
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};

export default WeatherItem;

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 10,
  },
  info: {
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingVertical: 20,
  },
  infoTitle: {
    fontSize: 18,
    color: colors.lightgray,
  },
  infoText: {
    fontSize: 16,
    color: colors.white,
    paddingTop: 4,
  },
  city: {
    color: colors.white,
    fontSize: 36,
  },
  image: {
    width: 280,
    height: 150,
    alignContent: 'center',
  },
  temperature: {
    fontSize: 48,
    color: colors.white,
    paddingBottom: 8,
  },
  temperatureText: {
    borderRadius: 20,
    textAlign: 'center',
    padding: 4,
    backgroundColor: colors.night.pink,
    color: colors.white,
    elevation: 10,
  },
  card1: {
    padding: 10,
    borderRadius: 12,
    backgroundColor: 'rgba(52, 52, 52, 0.2)',
    opacity: 0.9,
  },
  card2: {
    padding: 10,
    width: '40%',
    borderRadius: 12,
    borderWidth: 1,
    borderColor: colors.white,
  },
  cardTitle: {
    fontSize: 20,
    color: colors.white,
  },
  cardInfo: {
    fontSize: 24,
    color: colors.white,
  },
  iconContainer1: {
    paddingVertical: 8,
  },
  iconContainer2: {
    paddingVertical: 20,
  },
  icon1: {
    width: 24,
    height: 24,
  },
  icon2: {
    width: 32,
    height: 32,
  },
});
