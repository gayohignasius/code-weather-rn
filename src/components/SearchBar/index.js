import {
  StyleSheet,
  View,
  TextInput,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import React from 'react';
import { colors } from '../../constans/colors';
import { SearchIcon } from '../../assets';

const SearchBar = ({ value, handleSearchBar, onPress }) => {
  return (
    <View style={styles.searchBarContainer}>
      <TextInput
        value={value}
        placeholder="City Name"
        placeholderTextColor={colors.lightgray}
        onChangeText={handleSearchBar}
        style={styles.searchBar}
      />
      <TouchableOpacity style={styles.searchButton} onPress={onPress}>
        <Image source={SearchIcon} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
};

export default SearchBar;

const styles = StyleSheet.create({
  searchBarContainer: {
    marginTop: 20,
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
  },
  searchBar: {
    padding: 10,
    borderWidth: 1,
    borderRadius: 20,
    borderColor: colors.lightgray,
    width: '100%',
  },
  searchButton: { position: 'absolute', right: 20, paddingRight: 10 },
  icon: {
    width: 20,
    height: 20,
  },
});
