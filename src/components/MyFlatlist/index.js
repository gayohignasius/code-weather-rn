import { FlatList, StyleSheet, Text, View, Image } from 'react-native';
import React from 'react';
import { colors } from '../../constans/colors';

const MyFlatlist = ({ forecast }) => {
  return (
    <View style={{ paddingTop: 20 }}>
      <Text style={{ fontSize: 24, color: colors.white }}>Hourly Forecast</Text>
      <View
        style={{
          borderBottomColor: colors.white,
          borderBottomWidth: StyleSheet.hairlineWidth,
        }}
      />
      <FlatList
        horizontal
        data={forecast}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item }) => {
          const dt = new Date(item.dt * 1000);
          const weather = item.weather[0];
          return (
            <View style={styles.hour}>
              <Text style={{ color: colors.white }}>
                {dt.toLocaleTimeString().replace(/:\d+ /, ' ')}
              </Text>
              <Image
                style={{ width: 60, height: 40 }}
                source={{
                  uri: `https://openweathermap.org/img/wn/${weather.icon}@4x.png`,
                }}
              />
              <Text style={{ color: colors.white }}>
                {Math.round(item.main.temp)}°C
              </Text>
              <Text style={{ color: colors.white }}>{weather.description}</Text>
            </View>
          );
        }}
      />
    </View>
  );
};

export default MyFlatlist;

const styles = StyleSheet.create({
  hour: {
    alignItems: 'center',
    padding: 10,
  },
});
