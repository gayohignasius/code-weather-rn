import React, { useCallback, useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  TextInput,
  FlatList,
  RefreshControl,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { getWeatherAsync } from '../features/weatherSlice';
import { getForecastAsync } from '../features/forecastSlice';
import { colors } from '../constans/colors';
import { MyFlatlist, SearchBar, WeatherItem } from '../components';

const Home = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    getWeatherData(searchBar);
    getForecastData(searchBar);
  }, [getWeatherData, getForecastData, refreshing, searchBar]);

  const getWeatherData = searchValue => {
    dispatch(getWeatherAsync(searchValue));
  };

  const getForecastData = searchValue => {
    dispatch(getForecastAsync(searchValue));
  };

  const { weather } = useSelector(state => state.weather);
  const { forecast } = useSelector(state => state.forecast);
  const loadingStatus = useSelector(state => state.weather.status);
  const [searchBar, setSearchBar] = useState('Jakarta');
  const [refreshing, setRefreshing] = useState(false);
  let icon = '';

  useEffect(() => {
    if (Object.keys(weather).length != 0) icon = weather.weather[0].icon;
  }, [weather]);

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  }, []);

  const handleSearchBar = text => {
    setSearchBar(text);
  };

  const handleSearchClicked = () => {
    dispatch(getWeatherAsync(searchBar));
    dispatch(getForecastAsync(searchBar));
  };

  if (loadingStatus === 'loading') {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color={colors.blue} />
      </View>
    );
  }

  if (loadingStatus === 'fulfilled') {
    return (
      <View style={styles.container(icon)}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          {/* search bar */}
          <SearchBar
            value={searchBar}
            onPress={handleSearchClicked}
            handleSearchBar={handleSearchBar}
          />
          {/* content */}
          <View style={styles.content}>
            <WeatherItem weather={weather} />
            {/* bottom content */}
            <MyFlatlist forecast={forecast} />
          </View>
        </ScrollView>
      </View>
    );
  }
};

export default Home;

const styles = StyleSheet.create({
  container: icon => {
    return {
      flex: 1,
      backgroundColor: icon.includes('n') ? colors.night.blue : colors.day,
    };
  },
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    marginTop: 10,
    paddingHorizontal: 20,
  },
});
